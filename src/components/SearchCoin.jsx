import React from 'react';

const Search = (props)=>{


    return (
        <div className="search">
            <label htmlFor="">Search Coin</label>
            <input type="text" onChange={event => props.filter(event.target.value)} name="" placeholder='Search Coin' id=""/>
        </div>
    )
}

export default Search;