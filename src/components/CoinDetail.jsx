import React from 'react';


const CoinDetail = (props)=>{

    return(
        <div className='coin-detail'>
            <div>{props.coinDetails.symbol}</div>
            <div>{props.coinDetails.price}</div>
        </div>
    )
}

export default CoinDetail;