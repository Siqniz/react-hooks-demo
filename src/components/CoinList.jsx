import React from 'react';

const CoinList =(props)=>{


    if(props.coins == null){
        return <div>Error Loading</div>
    } else {
    return (
        <div>
            {props.coins?.map(c=>{
                return (<div className="coin" key={c.symbol} onClick={()=> props.displayCoin(c)}>
                    <div>{c.symbol}</div>
                    <div>{c.price}</div>
                </div>)
                }
            )}
        </div>
        
    )
    }
}


export default CoinList;