import React, { useEffect, useState } from 'react';

import Search from './components/SearchCoin';
import Coinlist from './components/CoinList';
// import Coin from './components/Coin';
import CoinDetail from './components/CoinDetail';
import './App.css';

function App() {
	const [coinlist, setCoinlist] = useState([]);
	const [filteredCoinlist, setFilteredCoinlist] = useState([]);
	const [coinDetails, setCoinDetails] = useState({});
	// const [errorMsg, setErrorMsg] = useState('');

	useEffect(() => {
		fetch(`https://coinlib.io/api/v1/coinlist/?key=`)
			.then((response) => {
				return response.json();
			})
			.then((data) => {
				const { coins } = data;
				coins
					? setCoinlist(coins)
					: setCoinlist([{ symbol: 'ETH', price: '1.99' }]);
				setFilteredCoinlist(coins);
			});
	}, [setCoinDetails, setFilteredCoinlist]);

	const displayCoinDetailHandler = (coinDetails) => {
		setCoinDetails(coinDetails);
	};

	const filterCoins = (query) => {
		console.log(query);
		const filtered = [...coinlist];
		const result = filtered.filter((_f) => {
			const regex = new RegExp(`^${query}`, 'i');
			return regex.test(_f['symbol']);
		});
		setFilteredCoinlist(result);
	};

	return (
		<div className="App">
			<div>
				<div>
					<Search filter={filterCoins}></Search>
				</div>
				<div className="coins">
					<Coinlist
						displayCoin={displayCoinDetailHandler}
						coins={filteredCoinlist}
						// error={errorMsg}
					></Coinlist>
					<div className="display-coin">
						<CoinDetail coinDetails={coinDetails}></CoinDetail>
					</div>
				</div>
			</div>
		</div>
	);
}

export default App;
